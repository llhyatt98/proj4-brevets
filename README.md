# Project 4: Brevet time calculator with Ajax

This project implements a Brevet time calculator using AJAX. The algorith can be
found inside of the acp_times.py module. The server side is implemented within the flask_brevets.py and the client side is found within templates/calc.html.

Testing for this project is a nose suite contained within the brevets_test.py module
found within the brevets directory. Tests can be ran via terminal using the 'nosetests' command. There is no testing associated with the docker container... these tests are meant to verify the accuracy for the algorithm. 

The program can be run using the run shell script in the brevet directory. Brevet control times rely on the following table for calculation:

![Table](./table.png)

For questions, email llh@uoregon.edu